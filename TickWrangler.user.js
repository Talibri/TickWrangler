// ==UserScript==
// @name         Talibri Tick Wrangler
// @namespace    http://tampermonkey.net/
// @version      2.2.2
// @description  Normalize tick rates
// @author       Gnomez
// @match        *://*.talibri.com/*
// @grant        none
// ==/UserScript==

window.tickWrangler = {
  stats: [],
  nuclearThreshold: 15000,
  timeoutThreshold: 3000
};

window.combat_ticker = null; // fix failure on initial combat start request

(function() {
  'use strict';
  
  var myIsActive = false;

  var mySkillReady = false;
  var mySkillInterval;
  
  var myPreviousPing = 0;
  var myPreviousRequestTime = 0;
  
  var myLeftToDump = 0;
  
  var myRetry = false;
  var myConsecutiveRetries = 0;
  var myRetryTimer;
  var myFailTimer;

  var myNuclearTimer;

  var mySkillTimerResetInterval;

  var myTheThing;
  var myThingArgs = [];
  var myLastCombatZone;

  var myNumRequests;
  var myNumRetries;
  var myNumRedText;
  var myTotalPing;
  var myStartTime;
  
  var myCurrentRequest = null;
  
  window.whatsMyTickrate = function() {
    var ticks = 0;
    var time = 0;
    var requests = 0;
    var ping = 0;
    tickWrangler.stats.forEach(function(session) {
      ticks += session.requests - session.redText;
      time += session.time;
      requests += session.requests;
      ping += session.totalPing;
    });
    if (myNumRequests !== undefined) {
      ticks += myNumRequests - myNumRedText;
      time += Date.now() - myStartTime;
      requests += myNumRequests;
      ping += myTotalPing;
    }
    
    var tickrate = time / ticks / 1000;
    var avgPing = ping / requests / 1000;
    return time === 0 ? "Hard to say..." : `${tickrate} seconds per tick; ${avgPing} average response time`;
  };
  
  function handleResponse(data) {
    myCurrentRequest = null;
    if (!myIsActive) return;
    
    myNumRequests++;
    
    if (data === "OK") {
      stopTheThing();
      return;
    }
    
    if (myTheThing.checkRedText(data)) {
      myRetry = true;
      myNumRedText++;
    }
    
    var newPing = Date.now() - myPreviousRequestTime;
    if (myRetry) {
      myRetry = false;
      myConsecutiveRetries++;
      clearInterval(mySkillInterval);
      let retryDelay = Math.max(0, myPreviousPing - 2 * newPing); // delay after extreme ping dips
      let retryDelayMod = myConsecutiveRetries > 3 ? 100 * myConsecutiveRetries : 0; // slow down after many consecutive failures
      myRetryTimer = setTimeout(retry, retryDelay + retryDelayMod);
      myNumRetries++;
    }
    else {
      myConsecutiveRetries = 0;
    }
    myPreviousPing = newPing;
    myTotalPing += newPing;
  }

  function handleFailure() {
    myCurrentRequest = null;
    myLeftToDump++;
    if (!myIsActive) return;
    
    myNumRequests++;
    
    stopSkill();
    stopAdventure();
    bootTheThing();
    myFailTimer = setTimeout(startTheThing, myTheThing.period - tickWrangler.timeoutThreshold);
    myNumRedText++;
  }

  function onNuclearTimer() {
    if (!myIsActive) return;
    
    stopSkill();
    stopAdventure();
    bootTheThing();
    startTheThing();
  }
  
  function retry() {
    if (!myIsActive) return;
    
    mySkillInterval = setInterval(triggerTheThing, myTheThing.period);
    triggerTheThing();
  }

  function startTheThing() {
    if (!myIsActive) return;
    
    mySkillInterval = setInterval(triggerTheThing, myTheThing.period);
    triggerTheThing();
  }
  
  function triggerTheThing() {
    mySkillReady = true;
    myTheThing.thing.apply(null, myThingArgs);
  }
  
  function doTheThing(xhr, settings) {
    if (!myTheThing) return;
    
    if (myCurrentRequest) mySkillReady = false;
    if (!mySkillReady) return dumpUnwantedRequest(xhr, settings);
    
    myCurrentRequest = xhr;
    
    clearTimeout(myNuclearTimer);
    myNuclearTimer = setTimeout(onNuclearTimer, tickWrangler.nuclearThreshold);
    clearTimeout(myRetryTimer);
    clearTimeout(myFailTimer);
    
    myPreviousRequestTime = Date.now();
    window.requestStart = myPreviousRequestTime + 10000; // avoid native latency compensation
    
    settings.timeout = tickWrangler.timeoutThreshold;
    xhr.done(myTheThing.done).fail(myTheThing.fail);
    mySkillReady = false;
  }

  function bootTheThing() {
    clearInterval(mySkillInterval);
    clearTimeout(myNuclearTimer);
    clearTimeout(myRetryTimer);
    clearTimeout(myFailTimer);
    
    myRetry = false;
    myConsecutiveRetries = 0;
    myPreviousPing = 0;
    
    myNumRequests = 0;
    myNumRetries = 0;
    myNumRedText = 0;
    myTotalPing = 0;
    myStartTime = Date.now();
    
    clearInterval(mySkillTimerResetInterval);
    mySkillTimerResetInterval = setInterval(function() {
      clearTimeout(window.skill_timer);
      clearTimeout(window.combat_timer);
    }, 1000);
    
    myIsActive = true;
  }
  
  function pickUpTheThing(task, xhr, settings) {
    bootTheThing();
    
    myTheThing = task;
    myThingArgs = task.getArgs(xhr, settings);
    
    mySkillReady = true;
    doTheThing(xhr, settings);
    mySkillInterval = setInterval(triggerTheThing, myTheThing.period);
  }
  
  function hotSwap(task, xhr, settings) {
    stopSkill();
    stopAdventure();
    pickUpTheThing(task, xhr, settings);
  }

  function stopTheThing() {
    if (!myIsActive) return;
    
    myIsActive = false;
    mySkillReady = false;
    
    clearInterval(mySkillInterval);
    clearTimeout(myNuclearTimer);
    clearTimeout(myRetryTimer);
    clearTimeout(myFailTimer);
    
    myRetry = false;
    myConsecutiveRetries = 0;
    
    clearInterval(mySkillTimerResetInterval);
    
    tickWrangler.stats.push({
      time: Date.now() - myStartTime,
      requests: myNumRequests,
      retries: myNumRetries,
      redText: myNumRedText,
      totalPing: myTotalPing
    });
  }
  let oldStopSkill = window.stopSkill;
  window.stopSkill = function() {
    if (myTheThing && myTheThing.isAdventure) return; // avoid resetting every time startAdventure is retried
    
    stopTheThing();
    oldStopSkill();
  };
  let oldStopAdventure = window.stopAdventure;
  window.stopAdventure = function() {
    stopTheThing();
    oldStopAdventure();
  }
  
  let tasks = [
    {
      pattern: /\/skills\/[^\/]*\/start_gathering.js$/,
      thing: processSkill,
      period: 5000,
      getArgs: function(xhr, settings) {
        let params = getQueryParams(settings.data);
        return [
          params.skill_id,
          params.milestone_item_id,
          params.location_id
        ];
      },
      checkRedText: function(data) {
        return data.length < 300;
      },
      done: handleResponse,
      fail: handleFailure
    },
    {
      pattern: /\/craft$/,
      thing: craft,
      period: 5000,
      getArgs: function(xhr, settings) {
        let params = getQueryParams(settings.data);
        return [
          params.recipe_id,
          params.quantity,
          params.crafts_remaining,
          params.crafting_spot_id
        ];
      },
      checkRedText: function(data) {
        return data.includes('Something went wrong please refresh the page and try again.');
      },
      done: handleResponse,
      fail: handleFailure
    },
    {
      pattern: /\/crafting\/[^\/]*\/component$/,
      thing: craftComponent,
      period: 5000,
      getArgs: function(xhr, settings) {
        let params = getQueryParams(settings.data);
        return [
          params.recipe_id,
          params.quantity,
          params.crafts_remaining,
          params.crafting_spot_id,
          params.auto_deconstruct_enabled,
          params.auto_deconstruct_min
        ];
      },
      checkRedText: function(data) {
        return data.includes('You tried to use the skill too quickly after your last tick! Please wait a few seconds and try again.');
      },
      done: handleResponse,
      fail: handleFailure
    },
    {
      isAdventure: true,
      pattern: /adventure\/(start|continue)$/,
      thing: function(e) {
        e !== undefined ? battle(e) : startAdventure(myLastCombatZone);
      },
      period: 6000,
      getArgs: function(xhr, settings) {
        let params = getQueryParams(settings.data);
        myLastCombatZone = params.combat_zone_id;
        // return undefined
      },
      checkRedText: function(data) {
        return data.length < 200 ||
               data.includes('If the combat update failed then clear timers');
      },
      done: function(data) {
        if (myThingArgs === undefined) {
          let match = /battle\([^\)]*/.exec(data);
          match && (myThingArgs = [
            match[0].substring(7)
          ]);
        }
        return handleResponse(data);
      },
      fail: function(xhr, textStatus, e) {
        if (xhr.status === 500) { // combat errors on early continue
          myLeftToDump++;
          return myTheThing.done('Silly server, that\'s not an error!');
        }
        else {
          return handleFailure();
        }
      }
    }
  ];
  function getQueryParams(query) {
    let out = {};
    
    let i = query[0] === '?' ? 0 : -1;
    while (i < query.length) {
      let keyStart = i;
      out[query.substring(++i, i = query.indexOf('=', i))] = query.substring(++i, (i = query.indexOf('&', i), i = i == -1 ? undefined : i)).replace(/\+/g, ' ');
    }
    
    return out;
  }
  
  function interceptTickRequest(e, xhr, settings) {
    tasks.forEach(function(task) {
      if (task.pattern.test(settings.url)) {
        if (!myIsActive) {
          if (myLeftToDump > 0) {
            dumpUnwantedRequest(xhr, settings);
          } else {
            pickUpTheThing(task, xhr, settings);
          }
        }
        else if (task === myTheThing) {
          doTheThing(xhr, settings);
        }
        else {
          hotSwap(task, xhr, settings);
        }
      }
    });
  }
  function dumpUnwantedRequest(xhr, settings) {
    // settings.url = '/notifications/read';
    // settings.data = undefined;
    myLeftToDump > 0 && myLeftToDump--;
    throw "Tick Wrangler does not approve.";
  }
  $(document).ajaxSend(interceptTickRequest);

})();